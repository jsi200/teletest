#
# Provider Configuration
#

provider "aws" {
  region                  = "eu-west-1"
  version                 = ">= 2.38.0"
  shared_credentials_file = "~/.aws/credentials"
}
