#
# EC2 VM for build and deploy
#

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_eip" "workstation" {
  instance = "${aws_instance.workstation.id}"
  vpc      = true
}

resource "aws_security_group" "workstation" {
  name   = "allow-all-sg"
  vpc_id = "${aws_vpc.demo.id}"
  ingress {
    cidr_blocks = [
      "0.0.0.0/0"
    ]
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
  }

  ingress {
    cidr_blocks = [
      "0.0.0.0/0"
    ]
    from_port = 8
    to_port   = 0
    protocol  = "icmp"
  }

  // Terraform removes the default rule
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# resource "aws_key_pair" "inbox-test" {
#   key_name = "rvg-inbox-test"
#   public_key = "${var.public_key}"
# }

# resource "aws_key_pair" "deployer" {
#   key_name   = "deployer-key"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBrViZnzRbzVB2bmfEM5aU6HDHrMRs7nyuxCh6Sz+Nj8oVBLLDSp6s5ummHLX/gTva7/Wh3ztzd67nrTK6LwYuez6/+QfDh/CV1HOu1pPKjLnxKHGzLzinkgiNoQ57xG3SS51e3q7ncApyA6Bhy8bbtBWRSvB3wY+UpU/2XrQGUadbGkNbFVJeoiuouR45V9nWxUGr0/N4YQC9WXMhhKW14cqCJnx7XuOqTJBQpfQ0pJy0RuWXo12V9h5mFjvkqXUe5Y7SRbN3YhIRcfYpIx2inyxM/U4oo8uRTOalpGuNj1BQM6yQ1/M1SZKSI1ga/2a8iISFK2zoOKk4erXhT5u7 jsi@mac-1.home"
# }

resource "aws_instance" "workstation" {
  ami                    = "${data.aws_ami.ubuntu.id}"
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.workstation.id
  vpc_security_group_ids = ["${aws_security_group.workstation.id}"]
  key_name               = "aws_key_pair"

}

#
# ECR Repository
#

# resource "aws_ecr_repository" "foo" {
#   name                 = "bar"
#   image_tag_mutability = "MUTABLE"

#   image_scanning_configuration {
#     scan_on_push = true
#   }
# }
