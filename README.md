***
# Exercise 1

## Presentation
In this Exercise we deploy a SpringBoot Java microservice application containing a REST API to a Kubernetes cluster running in AWS EKS using Infrastructure as Code.

The tools needed locally for this exercise1 are:

* Terraform 0.12.8
* Ansible 2.9.6
* configured AWS CLI 2.0.2

## 1)Deploy the AWS infrastructure

Resources about to be created in AWS:
![AWS Diagram](png/diag.png)

* 1 VPC
* 3 subnets
* 1 cluster EKS
* 1 VM Workstation
* sec rules


1)Run Terraform in [Exercise1/terraform](Exercise1/terraform):

```
cd Exercise1/terraform
terraform init
terraform apply --auto-approve
```

*terraform runs for ~15mins*

output tail:
```
(...)
aws_eks_node_group.demo: Still creating... [1m50s elapsed]
aws_eks_node_group.demo: Still creating... [2m0s elapsed]
aws_eks_node_group.demo: Creation complete after 2m9s [id=terraform-eks-demo:demo]

Apply complete! Resources: 22 added, 0 changed, 0 destroyed.
➜ terraform (master) ✗
```

## 2)Configure the workstation with Ansible and deploy the app on EKS

We use Ansible. [Exercise1/ansible/playbook.yml](Exercise1/ansible/playbook.yml) automated all the steps:

* Configure the worstation with Ansible
* Connect the EKS Cluster with kubectl
* Deploy the app with Helm

a)prepare the file [Exercise1/ansible/hosts](Exercise1/ansible/hosts):
```
(...)
ansible_private_key_file="~/Desktop/aws_key_pair.pem" # your AWS Key pair
(...)

(...)
34.255.226.149  # public IP of the workstation we have just created
```
b)export AWS creadentials
```
➜ ansible (master) ✗ export AWS_SECRET_KEY=...
➜ ansible (master) ✗ export AWS_ACCESS_KEY=...
```

c)finally run Ansible:
```
cd Exercise1/ansible
ansible-playbook -i ./hosts playbook.yml -e aws_access_key_id=$AWS_ACCESS_KEY -e aws_secret_access_key=$AWS_SECRET_KEY
```
*ansible runs for ~4mins*

output tail:
```
(...)
TASK [Test microservice access] ************************************************************************************************************************************************
changed: [52.213.181.137]

TASK [debug] *******************************************************************************************************************************************************************
ok: [52.213.181.137] => {
    "msg": [
        "curl http://af7a6268862da11ea9d49021141646bf-1976901065.eu-west-1.elb.amazonaws.com:8090",
        "Hello API Update 10.0.0.165",
        "Success!"
    ]
}

PLAY RECAP *********************************************************************************************************************************************************************
52.213.181.137             : ok=24   changed=18   unreachable=0    failed=0    skipped=0    rescued=0    ignored=1

➜ ansible (master) ✗
```
**Success!** means the REST API on EKS answers properly to the curl request.


***
***
***
***
# Exercise 2

## Presentation
We are going to make an API change to the java microservice and deploy the new version following Continuous Deployment Principles with a Canary Upgrade rollout in the Kubernetes cluster.

Below are generic solutions with kubernetes: one basic, one advanced involving the use of a Service Mesh. 

A first short paragraph covers the CD part. 

For the exercise2 the tools needed locally are:

  * Helm 2
  * configured AWS CLI 2.0.2
  * Kubectl 

## CI part: Automation with BitBucket Pipelines
We delegate here the build to BitBucket pipelines with this code [Exercise2/RestApiCD/bitbucket-pipelines.yml](Exercise2/RestApiCD/bitbucket-pipelines.yml).

## Kubernetes Canary Deployment using Service Selectors
We use Helm and Kubernetes labels to deploy a canary on the setup of Exercise1. If you deployed the infra above (that would be awesome!) you can ssh the workstation again.

![Simple Canary](png/simple_canary.png)

<THEORY\>

In kubernetes a Service is backed by a group of Pods. These Pods are exposed through endpoints. Using the *Service Selector* and the pods *Labels* we can setup a canary deployment.

Service selector configuration:
```
ubuntu@ip-10-0-2-129:~/helmcharts$ kubectl get svc k8s-service -o yaml
apiVersion: v1
kind: Service
(...)
    protocol: TCP
    targetPort: http
  selector:
    app: teleservice
  sessionAffinity: None
  type: LoadBalancer
(...)
ubuntu@ip-10-0-2-129:~/helmcharts$
```
Pods labels configuration:
```
ubuntu@ip-10-0-2-129:~/helmcharts$ kubectl get pods geared-peacock-teleservice-8474c7cd59-tbw42 -o yaml
apiVersion: v1
kind: Pod
metadata:
  annotations:
    kubernetes.io/psp: eks.privileged
  creationTimestamp: "2020-03-10T21:53:41Z"
  generateName: geared-peacock-teleservice-8474c7cd59-
  labels:
    app: teleservice
    app.kubernetes.io/instance: geared-peacock
    app.kubernetes.io/name: teleservice
(...)
```
`app: teleservice` is the glue between the service and the pods.

see https://kubernetes.io/docs/concepts/services-networking/connect-applications-service/ for more

</THEORY\>

We start with a deployed stable version v1.0.0 of the Rest API:
```
ubuntu@ip-10-0-2-129:~/helmcharts$ helm ls
NAME        	REVISION	UPDATED                 	STATUS  	CHART             	APP VERSION	NAMESPACE
filled-ibis 	1       	Tue Mar 10 14:25:18 2020	DEPLOYED	loadbalancer-0.1.0	           	default
zeroed-snail	1       	Tue Mar 10 21:43:50 2020	DEPLOYED	teleservice-0.1.0 	           	default
ubuntu@ip-10-0-2-129:~/helmcharts$
```

There are 3 replicas of the service v1.0.0:
```
ubuntu@ip-10-0-2-129:~/helmcharts$ kubectl get pods -o wide
NAME                           READY   STATUS    RESTARTS   AGE     IP           NODE                                       NOMINATED NODE   READINESS GATES
teleservice-77bb5598c6-7vcz4   1/1     Running   0          2m48s   10.0.0.251   ip-10-0-0-159.eu-west-1.compute.internal   <none>           <none>
teleservice-77bb5598c6-lc4bv   1/1     Running   0          2m48s   10.0.0.238   ip-10-0-0-159.eu-west-1.compute.internal   <none>           <none>
teleservice-77bb5598c6-pgxxt   1/1     Running   0          2m48s   10.0.0.226   ip-10-0-0-159.eu-west-1.compute.internal   <none>           <none>
ubuntu@ip-10-0-2-129:~/helmcharts$
```
100% of the requests are served by v1.0.0:
```
ubuntu@ip-10-0-2-129:~/helmcharts$ curl http://af7a6268862da11ea9d49021141646bf-1976901065.eu-west-1.elb.amazonaws.com:8090
Hello API10.0.0.226
ubuntu@ip-10-0-2-129:~/helmcharts$ curl http://af7a6268862da11ea9d49021141646bf-1976901065.eu-west-1.elb.amazonaws.com:8090
Hello API10.0.0.226
ubuntu@ip-10-0-2-129:~/helmcharts$ curl http://af7a6268862da11ea9d49021141646bf-1976901065.eu-west-1.elb.amazonaws.com:8090
Hello API10.0.0.226
ubuntu@ip-10-0-2-129:~/helmcharts$ curl http://af7a6268862da11ea9d49021141646bf-1976901065.eu-west-1.elb.amazonaws.com:8090
Hello API10.0.0.251
```

We deploy a canary for 25% of the traffic:
```
ubuntu@ip-10-0-2-129:~/helmcharts$ helm install --set image.tag=1.0.1 --set replicaCount=1  teleservice
NAME:   geared-peacock
LAST DEPLOYED: Tue Mar 10 21:53:41 2020
NAMESPACE: default
STATUS: DEPLOYED

RESOURCES:
==> v1/Deployment
NAME                        AGE
geared-peacock-teleservice  0s

==> v1/Pod(related)
NAME                                         AGE
geared-peacock-teleservice-8474c7cd59-tbw42  0s

==> v1/ServiceAccount
NAME                        AGE
geared-peacock-teleservice  0s
```
```
ubuntu@ip-10-0-2-129:~/helmcharts$ kubectl get deploy
NAME                         READY   UP-TO-DATE   AVAILABLE   AGE
geared-peacock-teleservice   1/1     1            1           62s
teleservice                  3/3     3            3           10m
ubuntu@ip-10-0-2-129:~/helmcharts$
```

25% of the requests are now served by v1.0.1:
```
ubuntu@ip-10-0-2-129:~/helmcharts$ curl http://af7a6268862da11ea9d49021141646bf-1976901065.eu-west-1.elb.amazonaws.com:8090
Hello API10.0.0.251
ubuntu@ip-10-0-2-129:~/helmcharts$ curl http://af7a6268862da11ea9d49021141646bf-1976901065.eu-west-1.elb.amazonaws.com:8090
Hello API10.0.0.238
ubuntu@ip-10-0-2-129:~/helmcharts$ curl http://af7a6268862da11ea9d49021141646bf-1976901065.eu-west-1.elb.amazonaws.com:8090
Hello API Update10.0.0.83
ubuntu@ip-10-0-2-129:~/helmcharts$ curl http://af7a6268862da11ea9d49021141646bf-1976901065.eu-west-1.elb.amazonaws.com:8090
Hello API10.0.0.226
```
(Hello API = v1.0.0, Hello API Update = v1.0.1)

In a real situation we would have an infrastructure for monitoring and testing. Have the number of incidents increased yet ? If yes then we rollback the deployment with `helm delete geared-peacock`

If things went well we can roll forward the three replicas in v1.0.0 to v1.0.1 and check all the images are now in version v1.0.1:
```
ubuntu@ip-10-0-2-129:~/helmcharts$ helm upgrade --set image.tag=1.0.1 --set replicaCount=3 zeroed-snail teleservice

ubuntu@ip-10-0-2-129:~/helmcharts$ kubectl get deploy -o yaml | grep image:
        - image: zoralarouss/teletest:1.0.1
        - image: zoralarouss/teletest:1.0.1
ubuntu@ip-10-0-2-129:~/helmcharts$
```

The service is now v1.0.1 using a very simple canary. We can now delete the canary.
```

ubuntu@ip-10-0-2-129:~/helmcharts$ helm delete geared-peacock
release "geared-peacock" deleted
ubuntu@ip-10-0-2-129:~/helmcharts$ helm ls
NAME        	REVISION	UPDATED                 	STATUS  	CHART             	APP VERSION	NAMESPACE
filled-ibis 	1       	Tue Mar 10 14:25:18 2020	DEPLOYED	loadbalancer-0.1.0	           	default
zeroed-snail	2       	Tue Mar 10 22:00:55 2020	DEPLOYED	teleservice-0.1.0 	           	default
ubuntu@ip-10-0-2-129:~/helmcharts$

ubuntu@ip-10-0-2-129:~/helmcharts$ curl http://af7a6268862da11ea9d49021141646bf-1976901065.eu-west-1.elb.amazonaws.com:8090
Hello API Update10.0.0.238
ubuntu@ip-10-0-2-129:~/helmcharts$ curl http://af7a6268862da11ea9d49021141646bf-1976901065.eu-west-1.elb.amazonaws.com:8090
Hello API Update10.0.0.226
ubuntu@ip-10-0-2-129:~/helmcharts$ curl http://af7a6268862da11ea9d49021141646bf-1976901065.eu-west-1.elb.amazonaws.com:8090
Hello API Update10.0.0.226
```
100% of the requests are served by v1.0.1.

## Advanced Canary deployment with EKS, App mesh and Flagger

A [Service Mesh](https://en.wikipedia.org/wiki/Service_mesh) brings several benefits:

* security
* reliability
* observability

Service Mesh implementation examples: Istio (google), App Mesh (AWS), ...

It's probably be the way to go today, especially when you start from scratch an orchestrator implementation.

A Service Mesh facilitate Canary Deployments. Let's illustrate with an example using EKS/App Mesh/Helm/Flagger/Prometheus/Grafana (see Sources paragraph for detailled steps).

In our setup `podinfo-primary` with the stable app version has 4 replicas

```
➜ ~ k get deploy -n test
NAME                 READY   UP-TO-DATE   AVAILABLE   AGE
appmesh-gateway      1/1     1            1           21m
flagger-loadtester   1/1     1            1           23m
podinfo              0/0     0            0           24m
podinfo-primary      4/4     4            4           22m
➜ ~
```
We generate traffic with `siege` from a micro EC2 VM

`ubuntu@ip-172-31-5-191:~$ siege -d3 -c200 http://a256997bb638811ea8b1d0a55bc2b8d4-7fe0deb9d022f7ae.elb.eu-west-1.amazonaws.com`

A canary is triggered:
```
➜ ~ kubectl -n test set image deployment/podinfo \
podinfod=stefanprodan/podinfo:3.1.3
```

`podinfo` is the canary
```
➜ ~ k get deploy -n test
NAME                 READY   UP-TO-DATE   AVAILABLE   AGE
appmesh-gateway      1/1     1            1           28m
flagger-loadtester   1/1     1            1           29m
podinfo              2/2     2            2           30m
podinfo-primary      2/2     2            2           28m
➜ ~
```

Flagger proceeds with the canary analysis and ramps up progressively the traffic to the canary pods. It does so as long as the metrics in Prometheus are stable.

```
 Normal   Synced  16m (x2 over 38m)     flagger  Starting canary analysis for podinfo.test
  Normal   Synced  16m (x2 over 38m)     flagger  Pre-rollout check acceptance-test passed
  Normal   Synced  16m (x2 over 38m)     flagger  Advance podinfo.test canary weight 5
  Normal   Synced  15m (x2 over 37m)     flagger  Advance podinfo.test canary weight 10
  Normal   Synced  12m (x2 over 34m)     flagger  Advance podinfo.test canary weight 25
  Normal   Synced  8m39s                 flagger  Advance podinfo.test canary weight 45
  Normal   Synced  4m39s (x17 over 33m)  flagger  (combined from similar events): Promotion completed! Scaling down podinfo.test
```

Grafana view (left = primary, right = canary):
![png/flagger.png](png/flagger.png)


The successful canary analysis concludes with the replacement of all pods with the new version.

# Discussion

What is your current CI/CD setup in details so we can improve it?

# Clean up

```
eksctl delete cluster --name=appmesh \
--region=eu-west-1

cd Exercise1/terraform
terraform destroy --auto-approve
```


# Sources
- https://thepracticaldeveloper.com/2017/12/11/dockerize-spring-boot/ (Spring Boot rest API code)
- https://github.com/terraform-providers/terraform-provider-aws/tree/master/examples/eks-getting-started (EKS in terraform)
- https://itnext.io/safe-and-automation-friendly-canary-deployments-with-helm-669394d2c48a (simple canary)
- https://docs.flagger.app/install/flagger-install-on-eks-appmesh (flagger)
